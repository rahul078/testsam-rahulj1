﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FizBuzzLogic.Interface
{
    public interface IDayChecker
    {
        bool CheckDay(string Date);
    }
}
