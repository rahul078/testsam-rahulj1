﻿namespace FizBuzzLogic.Class
{
    using FizBuzzLogic.Interface;
    using System;

    public class DivisibilityByThree : IDivisibility
    {
        private readonly IDayChecker dayChecker;

        public DivisibilityByThree(IDayChecker dayChecker)
        {
            this.dayChecker = dayChecker;
        }

        public bool IsDivisible(int number)
        {
            return number % 3 == 0;
        }

        public string GetString()
        {

            return (dayChecker.CheckDay(DateTime.Now.DayOfWeek.ToString()) ? "Fizz" : "Wizz");
        }

    }
}
