﻿namespace FizBuzzLogic.Class
{
    using FizBuzzLogic.Interface;
    using System;

    public class DivisibilityByFive : IDivisibility
    {
        private readonly IDayChecker dayChecker;

        public DivisibilityByFive(IDayChecker dayChecker)
        {
            this.dayChecker = dayChecker;
        }

        public bool IsDivisible(int number)
        {
            return number % 5 == 0;
        }
        public string GetString()
        { 
            return (dayChecker.CheckDay(DateTime.Now.DayOfWeek.ToString()) ? "Buzz" : "Wuzz");
        }
    }
}
