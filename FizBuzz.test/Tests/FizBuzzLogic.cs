namespace Tests
{
    using FizBuzzLogic.Class;
    using FizBuzzLogic.Interface;
    using NUnit.Framework;
    using System.Collections.Generic;

    public class Tests
    {
        ILogic logic;
        [SetUp]
        public void Setup()
        {
            logic = new Logic();
        }

        [TestCase(0,null)]
        [TestCase(1001, null)]
        [TestCase(-5, null)]
        public void InvalidInputTest(int number, List<string> expectedOutput)
        {
            //Act
            var actualOutput = logic.getList(number);

            //Assert
            Assert.AreEqual(expectedOutput, actualOutput);

        }
        
        [Test]
        public void ValidInputForFizz()
        {
            //Arrange
            var expectedOutput = new List<string> { "1", "2", "Fizz" };
            
            //Act
            var actualOutput = logic.getList(3);

            //Assert
            Assert.AreEqual(expectedOutput, actualOutput);
        }

        [Test]
        public void ValidInputForBuzz()
        {
            //Arrange
            var expectedOutput = new List<string> { "1", "2", "Fizz","4", "Buzz", "Fizz", "7" };

            //Act
            var actualOutput = logic.getList(7);

            //Assert
            Assert.AreEqual(expectedOutput, actualOutput);
        }

        [Test]
        public void ValidInputForFizBuzz()
        {
            //Arrange
            var expectedOutput = new List<string> { "1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "Fizz Buzz" };

            //Act
            var actualOutput = logic.getList(15);

            //Assert
            Assert.AreEqual(expectedOutput, actualOutput);
        }
    }
}