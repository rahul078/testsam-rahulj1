﻿namespace FizbuzzWeb.Controllers
{
    using System.Web.Mvc;
    using FizBuzzLogic.Interface;
    using FizbuzzWeb.Models;

    public class FizbuzzController : Controller
    {
        private readonly ILogic fizbuzzLogic;

        public FizbuzzController(ILogic logic)
        {
            this.fizbuzzLogic = logic;
        }

        // GET: Fizbuzz
        [HttpGet]
        public ActionResult Index()
        {
            return this.View();
        }

        [HttpPost]
        public ActionResult Index(Fizbuzz model)
        {
            if (this.ModelState.IsValid)
            {
                model.DisplayList = this.fizbuzzLogic.getList(model.InputNumer);
            }

            return this.View(model);
        }
    }
}