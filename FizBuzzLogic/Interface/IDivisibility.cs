﻿namespace FizBuzzLogic.Interface
{
    public interface IDivisibility
    {
        bool IsDivisible(int number);

        string GetString();
    }
}
