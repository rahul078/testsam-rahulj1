﻿namespace FizBuzzLogic.test.Tests
{
    using FizBuzzLogic.Class;
    using FizBuzzLogic.Interface;
    using Moq;
    using NUnit.Framework;
    using System.Collections.Generic;

    class DivisibilityByThreeTest
    {

        private Mock<IDayChecker> mockDayChecker;
        private DivisibilityByThree divisibilityByThree;

        [SetUp]
        public void Setup()
        {
            mockDayChecker = new Mock<IDayChecker>();
            divisibilityByThree = new DivisibilityByThree(mockDayChecker.Object);
        }

        [TestCase(true, "Wizz")]
        [TestCase(false, "Fizz")]
        public void CheckGetString(bool expectedOutput, string ResultString)
        {
            mockDayChecker.Setup(m => m.CheckDay(It.IsAny<string>())).Returns(expectedOutput);
            var actualOutput = divisibilityByThree.GetString();
            Assert.AreEqual(actualOutput, ResultString);
        }

        [TestCase(0, true)]
        [TestCase(9, true)]
        [TestCase(10, false)]
        public void InvalidInputTest(int number, bool expectedOutput)
        {
            //Act
            var actualOutput = divisibilityByThree.IsDivisible(number);

            //Assert
            Assert.AreEqual(expectedOutput, actualOutput);

        }
    }
}
