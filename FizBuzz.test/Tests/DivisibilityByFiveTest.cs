﻿namespace FizBuzzLogic.test.Tests
{
    using FizBuzzLogic.Class;
    using FizBuzzLogic.Interface;
    using Moq;
    using NUnit.Framework;
    using System.Collections.Generic;

    class DivisibilityByFiveTest
    {
        private Mock<IDayChecker> mockDayChecker;
        private DivisibilityByFive divisibilityByFive;

        [SetUp]
        public void Setup()
        {
            mockDayChecker = new Mock<IDayChecker>();
            divisibilityByFive = new DivisibilityByFive(mockDayChecker.Object);
        }

        [TestCase(true, "Wuzz")]
        [TestCase(false, "Buzz")]
        public void CheckGetString(bool expectedOutput, string ResultString)
        {
            mockDayChecker.Setup(m => m.CheckDay(It.IsAny<string>())).Returns(expectedOutput);
            var actualOutput = divisibilityByFive.GetString();
            Assert.AreEqual(actualOutput, ResultString);
        }

        [TestCase(0, true)]
        [TestCase(10, true)]
        [TestCase(21, false)]
        public void InvalidInputTest(int number, bool expectedOutput)
        {
            //Act
            var actualOutput = divisibilityByFive.IsDivisible(number);

            //Assert
            Assert.AreEqual(expectedOutput, actualOutput);

        }
    }
}
