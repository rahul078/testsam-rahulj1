﻿namespace FizBuzzLogic.Interface
{
    using System.Collections.Generic;

    public interface ILogic
    {
        List<string> getList(int number);
    }
}
