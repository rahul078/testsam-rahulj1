﻿namespace FizbuzzWeb.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Fizbuzz
    {
        [Display(Name = "Enter a number")]
        [Required(ErrorMessage = "*Enter a valid number")]
        [Range(1, 1000, ErrorMessage = "Enter the number between 1 and 1000")]
        public int InputNumer { get; set; }

        public List<string> DisplayList { get; set; }
    }
}